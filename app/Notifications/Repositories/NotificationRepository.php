<?php


namespace App\Notifications\Repositories;


use App\Notifications\Model\Notification;
use Infrastructure\Database\Repositories\RepositoryBase;

class NotificationRepository extends RepositoryBase
{
    protected string $model = Notification::class;

    public function getAll()
    {
        return Notification::query()->orderBy('id', 'desc')->get();
    }
}
