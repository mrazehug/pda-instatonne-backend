<?php


namespace App\Search;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::group(['prefix' => 'api', 'middleware' => 'auth:api'], base_path('app/Search/api_routes.php'));
    }
}
