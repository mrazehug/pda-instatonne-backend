<?php


namespace App\Search\Requests;


use Infrastructure\Http\Requests\RequestBase;

class SearchRequest extends RequestBase
{
    public function rules()
    {
        return [
            'lf' => [ 'sometimes', 'in:USER,HASHTAG' ],
            'q'  => [ 'sometimes', 'string:max:1024' ]
        ];
    }
}
