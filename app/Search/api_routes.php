<?php

use App\Search\Controllers\SearchController;
use Illuminate\Support\Facades\Route;

Route::get('/search', [ SearchController::class, 'search' ]);
