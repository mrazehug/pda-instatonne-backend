<?php


namespace App\Users\Requests;


use Infrastructure\Http\Requests\RequestBase;

class UpdateUserRequest extends RequestBase
{
    public function rules()
    {
        return [
            'username'      => [ 'sometimes', 'string', 'max:255' ],
            'email'         => [ 'sometimes', 'string', 'email', 'max:255' ],
            'bio'           => [ 'string', 'max:2048' ],
            'profile_photo' => [ 'file', 'mimes:jpeg,png,bmp,mp4,avi' ]
        ];
    }
}
