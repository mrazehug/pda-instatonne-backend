<?php


namespace App\Users\Repositories;


use Infrastructure\Database\Repositories\RepositoryBase;
use Infrastructure\Database\User;

class UserRepository extends RepositoryBase
{
    protected string $model = User::class;
}
