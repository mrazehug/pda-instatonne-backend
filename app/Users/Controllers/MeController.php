<?php


namespace App\Users\Controllers;


use App\Posts\Requests\StorePostRequest;
use App\Users\Requests\UpdateUserRequest;
use App\Users\Services\MeService;
use App\Users\Services\UserService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controllers\Controller;

/**
 * @group Me
 * Endpoint for managing currently authenticated user.
 *
 * @package App\Users\Controllers
 */
class MeController extends Controller
{
    protected UserService $userService;
    protected MeService $meService;

    public function __construct(UserService $userService, MeService $meService)
    {
        $this->userService = $userService;
        $this->meService = $meService;
    }

    /**
     * Me
     * Returns information about currently authenticated user.
     * @return mixed
     */
    public function me()
    {
        $me = $this->userService->show(Auth::user()->username, [ 'posts', 'posts.fileUpload', 'posts.comments', 'posts.likes', 'posts.user', 'followers', 'following' ]);
        $me->makeVisible('email');
        $me->append([ 'follower_count', 'following_count' ]);

        return $this->response($me);
    }

    /**
     * My feed
     * Returns feed of currently authenticated user.
     * @return mixed
     *
     * @responseFile responses/me/feed.json
     */
    public function feed()
    {
        // This is ugly, innit?
        $feed = Arr::flatten(Arr::pluck($this->userService->show(Auth::user()->username, [ 'following.posts.fileUpload', 'following.posts.user', 'following.posts.likes', 'following.posts.comments' ])->following, 'posts'));

        $unorderedPosts = collect($this->userService->show(Auth::user()->username, [ 'following.posts.fileUpload', 'following.posts.user', 'following.posts.likes', 'following.posts.comments' ])->following)->pluck('posts');
        // error_log(print_r($unorderedPosts, true));
        $unorderedPosts = Arr::flatten($unorderedPosts);


        foreach ($unorderedPosts as $feedItem)
        {
            $feedItem->append([ 'commentCount', 'likeCount' ]);
            $feedItem->makeHidden('likes');
        }


        return $this->response(collect($unorderedPosts)->sortByDesc('id')->values()->all());
    }

    /**
     * My notifications
     * Returns list of notifications for currently authenticated user.
     *
     * @responseFile responses/me/notifications.json
     *
     * @return mixed
     */
    public function notifications()
    {
        return $this->response($this->meService->getNotifications());
    }

    /**
     * Update me
     * Updates currently authenticated user. (Does not work yet)
     *
     * @responseFile responses/me/update.json
     *
     * @bodyParam username string New username of the user.
     * @bodyParam email string New username of the user.
     */
    public function updateMe(UpdateUserRequest $request)
    {
        $data = $request->getData();
//        error_log("data");
//        error_log(print_r($data, true));
        return $this->response($this->meService->updateMe($data));
    }

    /**
     * Store my post
     * Stores post of currently authenticated user
     *
     * @responseFile responses/me/store_post.json
     *
     * @bodyParam file file required The file of the post. Accepts only pictures now.
     * @bodyParam description string required The description of the post. Max length: 1024
     */
    public function storePost(StorePostRequest $request)
    {
        return $this->response($this->meService->storePost($request->getData()));
    }

    protected function response($data, int $statusCode = 200)
    {
        $data = [ 'data' => $data ];
        return parent::response($data, $statusCode); // TODO: Change the autogenerated stub
    }
}
