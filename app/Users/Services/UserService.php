<?php


namespace App\Users\Services;


use App\Notifications\Repositories\NotificationRepository;
use App\Users\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Infrastructure\Database\User;
use Infrastructure\Http\Services\ServiceBase;

class UserService extends ServiceBase
{
    protected string $identifierColumnName = 'username';
    protected NotificationRepository $notificationRepository;

    public function __construct(UserRepository $userRepository, NotificationRepository $notificationRepository)
    {
        $this->repository = $userRepository;
        $this->notificationRepository = $notificationRepository;
    }

    public function index(array $options)
    {
        $query = User::query();

        if(Arr::has($options, 'q'))
        {
            $q = $options['q'];
            $query->where('username', 'LIKE', "%$q%");
        }

        return $query->get()->append(['result_type', 'follower_count', 'following_count']);
    }

    public function follow($followerUsername, $followedUsername)
    {
        $follower = $this->getRequestedModel($followerUsername);
        $followed = $this->getRequestedModel($followedUsername);

        // If previously followed, unfollow
        if($followed->followers()->where('username', $followerUsername)->first()) {
            $followed->followers()->where('username', $followerUsername)->first()->pivot->notification()->delete();
            $followed->followers()->detach($follower);

            return false;
        }
        // If not previously followed, follow.
        else {
            $followed->followers()->save($follower);
            $follow = $followed->followers()->where('username', $followerUsername)->first()->pivot;

            $notification = $this->notificationRepository->create([]);
            $notification->user()->associate($followed);
            $follow->notification()->save($notification);

            $notification->save();

            return $notification->id;
        }
    }
}
