<?php


namespace App\Users\Services;


use App\Posts\Services\PostService;
use App\Users\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Infrastructure\FileUploads\Services\FileUploadService;
use Infrastructure\Http\Services\ServiceBase;

class MeService extends ServiceBase
{
    protected PostService $postService;
    protected FileUploadService $fileUploadService;

    public function __construct(UserRepository $userRepository, PostService $postService, FileUploadService $fileUploadService)
    {
        $this->repository = $userRepository;
        $this->postService = $postService;
        $this->fileUploadService = $fileUploadService;
    }

    public function updateMe(array $data)
    {
        $newMe = $this->update(Auth::id(), $data);

        if(Arr::has($data, 'profilePhoto'))
        {
            $photo = $this->fileUploadService->store([ 'file' => $data['profilePhoto'] ]);
            $newMe->profilePhoto()->associate($photo);
        }

        $newMe->save();
        return $newMe;
    }

    public function storePost(array $data)
    {
        $me = $this->getRequestedModel(Auth::id());
        $post = $this->postService->store($data);
        $me->posts()->save($post);

        return $post;
    }

    public function getNotifications()
    {
//        error_log("Getting notifications");
//        error_log(print_r($this->show(Auth::id())->notifications()->orderBy('id', 'desc')->get(), true));
        return $this->show(Auth::id())->notifications()->orderBy('id', 'desc')->get();
    }
}
