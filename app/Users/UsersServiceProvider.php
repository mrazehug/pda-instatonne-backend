<?php


namespace App\Users;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class UsersServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::group(['prefix' => 'api', 'middleware' => 'auth:api'],
            base_path('app/Users/api_routes.php'));
    }
}
