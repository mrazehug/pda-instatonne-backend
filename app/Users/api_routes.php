<?php

use App\Users\Controllers\MeController;
use App\Users\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/me', [ MeController::class, 'me' ]);
Route::put('/me', [ MeController::class, 'updateMe' ]);
Route::get('/me/feed', [ MeController::class, 'feed' ]);
Route::get('/me/notifications', [ MeController::class, 'notifications' ]);
Route::post('/me/posts', [ MeController::class, 'storePost' ]);

Route::get('/users/{username}', [ UserController::class, 'show' ]);
Route::post('/users/{username}/follow', [ UserController::class, 'follow' ]);

