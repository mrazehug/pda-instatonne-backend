<?php


namespace App\Users\Model;


use App\Notifications\Model\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\Database\User;

class Follow extends Pivot
{
    protected $table = 'follows';

    public function follower()
    {
        return $this->belongsTo(User::class, 'follower_id');
    }

    public function followed()
    {
        return $this->belongsTo(User::class, 'followed_id');
    }

    public function notification()
    {
        return $this->hasOne(Notification::class, 'follow_id');
    }
}
