<?php


namespace App\Posts\Requests;


use Infrastructure\Http\Requests\RequestBase;

class IndexPostsRequest extends RequestBase
{
    public function rules()
    {
        return [
            'q' => [ 'sometimes','string', 'max:1024']
        ];
    }
}
