<?php


namespace App\Posts\Requests;


use Infrastructure\Http\Requests\RequestBase;

class UpdatePostRequest extends RequestBase
{
    public function rules()
    {
        return [
            'description' => 'required|max:1024'
        ];
    }
}
