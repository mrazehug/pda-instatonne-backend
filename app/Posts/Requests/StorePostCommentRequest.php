<?php


namespace App\Posts\Requests;


use Infrastructure\Http\Requests\RequestBase;

class StorePostCommentRequest extends RequestBase
{
    public function rules()
    {
        return [
            'text' => [ 'required', 'string', 'max:2048' ]
        ];
    }
}
