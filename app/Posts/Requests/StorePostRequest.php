<?php


namespace App\Posts\Requests;


use Infrastructure\Http\Requests\RequestBase;

class StorePostRequest extends RequestBase
{
    public function rules()
    {
        return [
            'file' => [ 'required', 'file', 'mimes:jpeg,png,bmp,mp4,avi' ],
            'description' => [ 'required','string', 'max:1024' ]
        ];
    }
}
