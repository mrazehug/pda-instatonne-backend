<?php


namespace App\Posts\Model;


use App\Notifications\Model\Notification;
use Database\Factories\CommentFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\Database\User;

class Comment extends ModelBase
{
    use SoftDeletes, HasFactory;

    protected $fillable = [ 'text' ];
    protected $hidden = [ 'user_id', 'post_id', 'updated_at' ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function notification()
    {
        return $this->hasOne(Notification::class);
    }

    protected static function newFactory()
    {
        return CommentFactory::new();
    }
}
