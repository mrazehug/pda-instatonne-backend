<?php


namespace App\Posts\Model;


use App\Notifications\Model\Notification;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\Database\User;

class Like extends Pivot
{
    public $timestamps = false;
    protected $table = 'likes';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function notification()
    {
        return $this->hasOne(Notification::class, 'like_id');
    }
}
