<?php


namespace App\Posts\Model;


use Database\Factories\PostFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\Database\User;
use Infrastructure\FileUploads\Model\FileUpload;

class Post extends ModelBase
{
    use SoftDeletes, HasFactory;

    protected $fillable = [ 'description' ];
    protected $hidden = [ 'user_id', 'file_upload_id', 'deleted_at', 'updated_at' ];
    protected $appends = [ 'liked' ];
    // protected $with = [ 'comments', 'likes' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fileUpload()
    {
        return $this->belongsTo(FileUpload::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function mentions()
    {
        return $this->belongsToMany(User::class,'mentions');
    }

    public function hashtags()
    {
        return $this->belongsToMany(Hashtag::class, 'hashtags_posts');
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'likes')->withPivot('id')->using(Like::class);
    }

    public function getLikeCountAttribute()
    {
        return count($this->likes);
    }

    public function getLikedAttribute()
    {
        return $this->likes()->wherePivot('user_id', Auth::id())->first() != null;
    }

    public function getCommentCountAttribute()
    {
        return count($this->comments);
    }

    protected static function newFactory()
    {
        return PostFactory::new();
    }
}
