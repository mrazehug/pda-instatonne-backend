<?php


namespace App\Posts\Model;


use Database\Factories\HashtagFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Infrastructure\Database\Eloquent\ModelBase;

class Hashtag extends ModelBase
{
    use HasFactory;

    protected $fillable = ['text'];
    public $timestamps = false;
    protected $hidden = ['pivot'];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'hashtags_posts');
    }

    public function getResultTypeAttribute()
    {
        return "HASHTAG";
    }

    protected static function newFactory()
    {
        return HashtagFactory::new();
    }
}
