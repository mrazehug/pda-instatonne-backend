<?php


namespace App\Posts\Services;


use App\Notifications\Repositories\NotificationRepository;
use App\Posts\Model\Post;
use App\Posts\Repositories\CommentRepository;
use App\Posts\Repositories\HashtagRepository;
use App\Posts\Repositories\PostRepository;
use App\Users\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\FileUploads\Services\FileUploadService;
use Infrastructure\Http\Services\ServiceBase;

class PostService extends ServiceBase
{
    protected HashtagRepository $hashtagRepository;
    protected UserRepository $userRepository;
    protected FileUploadService $fileUploadService;
    protected CommentRepository $commentRepository;
    protected NotificationRepository $notificationRepository;

    public function __construct(PostRepository $postRepository,
                                HashtagRepository $hashtagRepository,
                                UserRepository $userRepository,
                                CommentRepository $commentRepository,
                                FileUploadService $fileUploadService,
                                NotificationRepository $notificationRepository)
    {
        $this->repository = $postRepository;
        $this->hashtagRepository = $hashtagRepository;
        $this->userRepository = $userRepository;
        $this->fileUploadService = $fileUploadService;
        $this->commentRepository = $commentRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Test candidate
     * @param $description
     * @return array
     */
    public function index(array $options)
    {
        $query = Post::query()->with([ 'user', 'fileUpload', 'hashtags' ]);

        if(Arr::has($options, 'q')) {
            $query->whereHas('hashtags', function ($query) use ($options) {
                $query->where('text', $options['q']);
            });
        }

        return $query->get();
    }

    /**
     * Test candidate
     * @param array $data
     * @return ModelBase
     */
    public function store(array $data): ModelBase
    {
        // Save basic post data
        $post = parent::store($data);

        // Save the file of the post
        $fileUpload = $this->fileUploadService->store(Arr::only($data, 'file'));
        $post->fileUpload()->associate($fileUpload);

        // Save hashtags
        $this->handleHashTags($post);

        return $post;
    }

    public function update($identifier, array $data): ModelBase
    {
        $post = parent::update($identifier, $data);
        $this->handleHashTags($post);

        return $post;
    }

    public function like($likerId, $id)
    {
        $liker = $this->userRepository->getWhere([['id', $likerId]])->firstOrFail();
        $post = $this->getRequestedModel($id, ['user']);
        $rv = false;

        if($post->likes()->where('username', $liker->username)->first())
        {
            $like = $post->likes()->where('username', $liker->username)->first()->pivot;
            $like->notification()->delete();
            $post->likes()->detach($liker);
        }
        else {
            $post->likes()->save($liker);

            $like = $post->likes()->where('username', $liker->username)->first()->pivot;

            $notification = $this->notificationRepository->create([]);
            $notification->user()->associate($post->user);
            $like->notification()->save($notification);
            $notification->save();

            $rv = true;
        }


        return $rv;
    }

    /**
     * Adds comment to post
     *
     * @param $likerId int id of the user that sent the comment
     * @param $postId int id of the post that shall be commented.
     * @param array $commentData Array with data of the comment to be added.
     * @return ModelBase Added comment.
     */
    public function comment($likerId, $postId, array $commentData)
    {
        $commenter = $this->userRepository->getWhere([['id', $likerId]])->first();
        $post = $this->getRequestedModel($postId);
        $comment = $this->commentRepository->create($commentData);

        $comment->user()->associate($commenter);
        $post->comments()->save($comment);

        $this->commentRepository->save($comment);

        $this->handleHashTags($post);

        $notification = $this->notificationRepository->create([]);
        $notification->user()->associate($post->user);
        $notification->comment()->associate($comment);
        $comment->save();

        $this->notificationRepository->save($notification);

        $this->repository->save($post);

        return $comment;
    }

    /**
     * Reevaluates hashtags of the post.
     *
     * @param $post
     */
    protected function handleHashTags($post)
    {
        $post->hashtags()->detach();
        $text = $post->description." ".implode(' ', Arr::pluck($post->comments, 'text'));
        $foundHashtags = collect($this->parseHashtags($text))->unique();

        foreach ($foundHashtags as $foundHashtag)
        {
            $hashtag = $this->hashtagRepository->getWhere([[ 'text', ltrim($foundHashtag, $foundHashtag[0]) ]])->first();
            if(!$hashtag && strlen($foundHashtag) - 1 <= 128)
            {
                $hashtag = $this->hashtagRepository->insert([ 'text' => ltrim($foundHashtag, $foundHashtag[0]) ]);
            }

            $post->hashtags()->save($hashtag);
        }
    }


    /**
     * Test candidate
     * @param $description
     * @return array
     */
    protected function parseHashtags($description) : array
    {
        $matches = [];
        preg_match_all("/#\w+/", $description, $matches);

        $hashtags = $matches[0];

//        foreach ($hashtags as $idx => $hashtag)
//        {
//            if(strlen($hashtag) == 1)
//                unset($hashtags[$idx]);
//        }

        return $hashtags;
    }
}
