<?php


namespace App\Posts\Services;


use App\Posts\Model\Hashtag;
use Illuminate\Support\Arr;
use Infrastructure\Http\Services\ServiceBase;

class HashtagService extends ServiceBase
{
    public function index(array $options)
    {
        $query = Hashtag::query()->with(['posts', 'posts.fileUpload', 'posts.likes', 'posts.comments', 'posts.user']);

        if(Arr::has($options, 'q'))
        {
            $q = $options['q'];
            $query->where('text', 'LIKE', "%$q%");
        }

        return $query->get()->append('result_type');
    }
}
