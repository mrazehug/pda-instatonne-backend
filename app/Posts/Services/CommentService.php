<?php


namespace App\Posts\Services;


use App\Posts\Repositories\CommentRepository;
use Infrastructure\Http\Services\ServiceBase;

class CommentService extends ServiceBase
{
    public function __construct(CommentRepository $commentRepository)
    {
        $this->repository = $commentRepository;
    }

    public function destroy($identifier)
    {
        $comment = $this->getRequestedModel($identifier);
        $comment->notification()->delete();
        $this->repository->delete($comment);
    }
}
