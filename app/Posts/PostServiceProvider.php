<?php


namespace App\Posts;


use App\Posts\Model\Comment;
use App\Posts\Model\Post;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Database\User;

class PostServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::group(['prefix' => 'api', 'middleware' => 'auth:api'],
            base_path('app/Posts/api_routes.php'));

        Gate::define('destroy-post', function (User $user, Post $post) {
            return $post->user->id == $user->id;
        });

        Gate::define('destroy-comment', function (User $user, Comment $comment) {
            return $comment->user->id == $user->id;
        });
    }
}
