<?php

use App\Posts\Controllers\CommentController;
use App\Posts\Controllers\PostController;
use Illuminate\Support\Facades\Route;

Route::get('/posts', [ PostController::class, 'index' ]);
Route::get('/posts/{id}', [ PostController::class, 'show' ]);
Route::delete('/posts/{id}', [ PostController::class, 'destroy' ]);
Route::post('/posts/{id}/like', [ PostController::class, 'like' ]);
Route::post('/posts/{id}/comment', [ PostController::class, 'comment' ]);

Route::delete('/comments/{id}', [ CommentController::class, 'destroy' ]);
