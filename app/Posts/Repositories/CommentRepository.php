<?php


namespace App\Posts\Repositories;


use App\Posts\Model\Comment;
use Infrastructure\Database\Repositories\RepositoryBase;

class CommentRepository extends RepositoryBase
{
    protected string $model = Comment::class;
}
