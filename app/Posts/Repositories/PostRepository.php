<?php


namespace App\Posts\Repositories;


use App\Posts\Model\Post;
use Infrastructure\Database\Repositories\RepositoryBase;

class PostRepository extends RepositoryBase
{
    protected string $model = Post::class;
}
