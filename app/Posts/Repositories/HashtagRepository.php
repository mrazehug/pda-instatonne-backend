<?php


namespace App\Posts\Repositories;


use App\Posts\Model\Hashtag;
use Infrastructure\Database\Repositories\RepositoryBase;

class HashtagRepository extends RepositoryBase
{
    protected string $model = Hashtag::class;
}
