<?php


namespace Database\Factories;


use App\Posts\Model\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        return [
            'description' => $this->faker->text
        ];
    }
}
