<?php


namespace Database\Factories;


use App\Posts\Model\Hashtag;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HashtagFactory extends Factory
{
    protected $model = Hashtag::class;


    public function definition()
    {
        return [ 'text' => Str::random(16) ];
    }
}
