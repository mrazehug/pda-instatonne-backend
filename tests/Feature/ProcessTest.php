<?php


namespace Tests\Feature;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Infrastructure\Database\User;
use Tests\TestCase;

class ProcessTest extends TestCase
{
    use DatabaseTransactions;

    protected function createUser()
    {
        return $this->post("/api/register", [
            "username" => Str::random(8),
            "email" => Str::random(8) . "@gmail.com",
            "password" => "12345678",
            "password_confirmation" => "12345678"
        ])->decodeResponseJson();
    }

    public function testCreateUserUpdate_checkMe()
    {
        $oneInfo = $this->post("/api/register", [
            "username" => "one",
            "email" => "one@gmail.com",
            "password" => "12345678",
            "password_confirmation" => "12345678"
        ])->decodeResponseJson();

        $this->assertDatabaseHas('users', [
            'email' => 'one@gmail.com'
        ]);

        $oneAccessToken = $this->post("/api/login", [
            "email" => "one@gmail.com",
            "password" => "12345678"
        ])->decodeResponseJson()["access_token"];

        $this->put("/api/me", [
            "email" => "new@gmail.com"
        ], ["Authorization" => "Bearer $oneAccessToken"]);

        $me = $this->get("/api/me", ["Authorization" => "Bearer $oneAccessToken"])->decodeResponseJson()["data"];

        self::assertEquals($me["email"], "new@gmail.com");
    }

    public function testCreateTwoUsersFollow_checkNotifications()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($other, 'api');
        $followResponse = $this->post("/api/users/".$one->username."/follow");
        $followResponse->assertStatus(200);
        $this->assertDatabaseHas('follows', [
            ["follower_id", $other->id],
            ["followed_id", $one->id]
        ]);

        $this->actingAs($one, 'api');
        $notificationResponse = $this->get("/api/me/notifications");
        $notificationResponse->assertStatus(200);
        $notificationResponse->assertJsonStructure([
            "data" => [
                "*" => [
                    "id", "created_at"
                ]
            ]
        ]);

        $notifications = $notificationResponse->decodeResponseJson()["data"];
        self::assertCount(1, $notifications);
    }

    public function testCreateTwoUsersFollowAddPost_checkFeed()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($other, 'api');

        $followResponse = $this->post("/api/users/$one->username/follow");
        $followResponse->assertStatus(200);

        $this->assertDatabaseHas('follows', [
            ["follower_id", $other->id],
            ["followed_id", $one->id]
        ]);

        $this->actingAs($one, 'api');

        $postResponse = $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "What a post! #hashtag"
        ]);
        $postResponse->assertStatus(200);
        $postResponse->assertJsonStructure([
            "data" => [
                "id", "description", "created_at", "liked", "file_upload"
            ]
        ]);

        $this->assertDatabaseHas('hashtags', [
            "text" => "hashtag"
        ]);

        $this->actingAs($other, 'api');
        $feedResponse = $this->get("/api/me/feed");

        $feedResponse->assertStatus(200);
        $feed = $feedResponse->decodeResponseJson()["data"];

        self::assertCount(1, $feed);
    }

    public function testCreateAndDeletePost_checkDeleted()
    {
        $oneInfo = $this->createUser();

        $oneAccessToken = $this->post("/api/login", [
            "email" => $oneInfo["email"],
            "password" => "12345678"
        ])->decodeResponseJson()["access_token"];

        $postId = $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "What a post! #hashtag"
        ], [
            "Authorization" => "Bearer $oneAccessToken"
        ])->decodeResponseJson()["data"]["id"];

        $deletePostResponse = $this->delete("/api/posts/$postId", ["Authorization" => "Bearer $oneAccessToken"]);
        $deletePostResponse->assertStatus(204);

        $postNotFoundResponse = $this->get("/api/posts/$postId");
        $postNotFoundResponse->assertStatus(404);
    }

    public function testCommentAndDeleteComment_checkNotificationDisappeared()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($one, "api");
        $postResponse = $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "What a post! #hashtag"
        ]);
        $postResponse->assertStatus(200);
        $postResponse->assertJsonStructure([
            "data" => [
                "description", "created_at", "id", "liked",
                "file_upload" => [
                    "id", "url", "type"
                ],
                "comments"
            ]
        ]);
        $this->assertDatabaseHas("hashtags", [
            "text" => "hashtag"
        ]);

        $this->actingAs($other, "api");
        $postId = $postResponse->decodeResponseJson()["data"]["id"];
        $commentResponse = $this->post("/api/posts/$postId/comment", [
            "text" => "Cool post! #cool #post #damn"
        ]);
        $commentResponse->assertStatus(200);
        $commentResponse->assertJsonStructure([
            "data" => [
                "text", "created_at", "id"
            ]
        ]);

        // error_log(print_r($commentResponse, true));

        $commentId = $commentResponse->decodeResponseJson()["data"]["id"];
        $this->assertDatabaseHas("comments", [
            "id" => $commentId
        ]);
        $this->assertDatabaseHas("notifications", [
            "user_id" => $one->id,
            "comment_id" => $commentId
        ]);

        $this->assertDatabaseHas("hashtags", [
            "text" => "cool"
        ]);
        $this->assertDatabaseHas("hashtags", [
            "text" => "post"
        ]);

        $this->actingAs($one, "api");
        $notificationsResponse = $this->get("/api/me/notifications");
        $notificationsResponse->assertStatus(200);
        $notificationsResponse->assertJsonStructure([
            "data" => [
                "*" => [
                    "id", "type", "created_at"
                ]
            ]
        ]);
        $notifications = $notificationsResponse->decodeResponseJson()["data"];

        self::assertCount(1, $notifications);
        self::assertEquals($notifications[0]["type"], "COMMENT");

        $this->actingAs($other, "api");
        $commentDeleteResponse = $this->delete("/api/comments/$commentId");
        $commentDeleteResponse->assertStatus(204);

        $this->assertDatabaseMissing("notifications", [
            "user_id" => $one->id,
            "comment_id" => $commentId
        ]);
    }

    public function testLikeAndRemoveLike_checkNotificationDisappears()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($one, "api");
        $postResponse = $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "What a post! #hashtag"
        ]);
        $postResponse->assertStatus(200);
        $postResponse->assertJsonStructure([
            "data" => [
                "description", "created_at", "id", "liked",
                "file_upload" => [
                    "id", "url", "type"
                ],
                "comments"
            ]
        ]);
        $this->assertDatabaseHas("hashtags", [
            "text" => "hashtag"
        ]);

        $this->actingAs($other, "api");
        $postId = $postResponse->decodeResponseJson()["data"]["id"];
        $commentResponse = $this->post("/api/posts/$postId/like");
        $commentResponse->assertStatus(200);
        $commentResponse->assertJson([
            "data" => true
        ]);

        // error_log(print_r($commentResponse, true));

        $this->assertDatabaseHas("likes", [
            "user_id" => $other->id,
            "post_id" => $postId
        ]);

        $this->actingAs($one, "api");
        $notificationsResponse = $this->get("/api/me/notifications");
        $notificationsResponse->assertStatus(200);
        $notificationsResponse->assertJsonStructure([
            "data" => [
                "*" => [
                    "id", "type", "created_at"
                ]
            ]
        ]);
        $notifications = $notificationsResponse->decodeResponseJson()["data"];

        self::assertCount(1, $notifications);
        self::assertEquals($notifications[0]["type"], "LIKE");

        $this->actingAs($other, "api");
        $postId = $postResponse->decodeResponseJson()["data"]["id"];
        $commentResponse = $this->post("/api/posts/$postId/like");
        $commentResponse->assertStatus(200);
        $commentResponse->assertJson([
            "data" => false
        ]);

        $this->assertDatabaseMissing("likes", [
            "user_id" => $other->id,
            "post_id" => $postId
        ]);
    }

    public function testFollowAndUnfollowLike_checkNotificationDisappears()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($other, 'api');
        $followResponse = $this->post("/api/users/".$one->username."/follow");
        $followResponse->assertStatus(200);
        $followResponse->assertJson([
            "data" => true
        ]);

        $this->assertDatabaseHas("follows", [
            "follower_id" => $other->id,
            "followed_id" => $one->id
        ]);

        $this->actingAs($one, 'api');
        $notificationsResponse = $this->get("/api/me/notifications");
        $notificationsResponse->assertStatus(200);
        $notificationsResponse->assertJsonStructure([
            "data" => [
                "*" => [
                    "id", "type", "created_at"
                ]
            ]
        ]);

        $notifications = $notificationsResponse->decodeResponseJson()["data"];
        self::assertCount(1, $notifications);
        self::assertEquals("FOLLOW", $notifications[0]["type"]);

        $this->actingAs($other, "api");
        $followResponse = $this->post("/api/users/".$one->username."/follow");
        $followResponse->assertStatus(200);
        $followResponse->assertJson([
            "data" => false
        ]);

        $this->assertDatabaseMissing("follows", [
            "follower_id" => $other->id,
            "followed_id" => $one->id
        ]);

        $this->actingAs($one, 'api');
        $notificationsResponse = $this->get("/api/me/notifications");
        $notificationsResponse->assertStatus(200);

        $notifications = $notificationsResponse->decodeResponseJson()["data"];
        self::assertCount(0, $notifications);
    }

    public function testSearchHashtag_validResults()
    {
        $one = User::factory()->create();
        $other = User::factory()->create();

        $this->actingAs($one, 'api');
        $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "Mad post! #hashtag1 #hashtag2"
        ])->decodeResponseJson()["data"];

        $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "Another mad post! #hashtag1"
        ])->decodeResponseJson()["data"];

        $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "Another mad post! #hashtag2"
        ])->decodeResponseJson()["data"];

        $this->post("/api/me/posts", [
            "file" => UploadedFile::fake()->image('avatar.jpg', 100, 100),
            "description" => "Another mad post! #hashtag3"
        ])->decodeResponseJson()["data"];

        $this->actingAs($other, 'api');
        $response = $this->get("/api/search?lf=HASHTAG&q=has");
        $response->assertStatus(200);

        $results = $response->decodeResponseJson()["data"];
        foreach ($results as $result)
        {
            self::assertTrue(str_starts_with($result["text"], "has"));
        }
    }
}
