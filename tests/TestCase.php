<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Creates specified repository mock and configures it accordingly.
     *
     * @param string $repositoryClassName
     * @param array $config
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    protected function mockRepository(string $repositoryClassName, array $config)
    {
        $repositoryMock = $this->getMockBuilder($repositoryClassName)->getMock();
        foreach ($config as $configItem)
        {
            try {
                $repositoryMock->expects($this->exactly($configItem['callCount']))
                    ->method($configItem['method'])->willReturn($configItem['rv']);
            }
            catch (\Exception $exception)
            {
                fwrite(STDERR, print_r("Repository mock setup config error: ".$exception->getMessage()));
            }
        }

        app()->instance($repositoryClassName, $repositoryMock);

        return $repositoryMock;
    }

    protected function getPublicCallableMethod($className, $methodName)
    {
        $method = new \ReflectionMethod($className, $methodName);
        $method->setAccessible(true);

        return $method;
    }
}
