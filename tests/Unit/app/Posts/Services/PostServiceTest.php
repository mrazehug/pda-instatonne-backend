<?php


namespace Tests\Unit\app\Posts\Services;

use App\Notifications\Model\Notification;
use App\Notifications\Repositories\NotificationRepository;
use App\Posts\Model\Comment;
use App\Posts\Model\Hashtag;
use App\Posts\Model\Post;
use App\Posts\Repositories\CommentRepository;
use App\Posts\Repositories\PostRepository;
use App\Posts\Services\PostService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;
use Infrastructure\Database\User;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Tests\TestCase;

class PostServiceTest extends TestCase
{
    use DatabaseTransactions;


    public function testUpdatePost_hashtagsChanged_addsHashtagsOk()
    {
        $post = Post::factory()->create();
        $expectedDescription = 'Desc #hashtag1 #hashtag2 #hashtag3 #hashtag4';
        $expectedHashtagTexts = [ 'hashtag1', 'hashtag2', 'hashtag3', 'hashtag4' ];

        $postRepositoryMock = $this->mockRepository(PostRepository::class, [[
            "callCount" => 1,
            "method" => "getById",
            "rv" => $post
        ]]);

        $postRepositoryMock->expects($this->exactly(1))->method('update')->willReturnCallback(function () use ($post, $expectedDescription) {
            $post->fill([ 'description' => $expectedDescription ])->save();
            return $post;
        });

        app()->instance(PostRepository::class, $postRepositoryMock);
        $postService = app()->make(PostService::class);

        $updatedPost = $postService->update($post->id, ['description' => $expectedDescription]);

        self::assertTrue($updatedPost->description == $expectedDescription);
        self::assertTrue(count($updatedPost->hashtags) == 4);
        foreach ($updatedPost->hashtags as $index => $hashtag)
        {
            self::assertEquals($hashtag->text, $expectedHashtagTexts[$index]);
        }
    }

    public function testCommentPost_noHashtagsBefore_addsHashtagsOk()
    {
        $expectedCommentText = 'Comment text #hashtag1 #hashtag2';
        $expectedHashtagTexts = [ 'hashtag1', 'hashtag2' ];
        $post = Post::factory()->make();
        $user = User::factory()->make();
        $comment = Comment::factory()->make();
        $notification = Notification::factory()->make();
        $comment->text = $expectedCommentText;

        $this->mockRepository(PostRepository::class, [[
            "callCount" => 1,
            "method" => 'getById',
            "rv" => $post
        ], [
            "callCount" => 1,
            "method" => 'save',
            "rv" => true
        ]]);

        $this->mockRepository(CommentRepository::class, [[
            "callCount" => 1,
            "method" => 'create',
            "rv" => $comment
        ], [
            "callCount" => 1,
            "method" => 'save',
            "rv" => true
        ]]);

        $this->mockRepository(NotificationRepository::class, [[
            "callCount" => 1,
            "method" => "create",
            "rv" => $notification
        ], [
            "callCount" => 1,
            "method" => "save",
            "rv" => true
        ]]);

        $post->save();
        $user->save();

        $postService = app()->make(PostService::class);
        $this->actingAs($user);

        $comment = $postService->comment($user->id, $post->id, [ 'text' => $expectedCommentText]);

        self::assertTrue(count($post->comments) == 1);
        self::assertTrue($comment->text == $expectedCommentText);
        self::assertTrue(count($post->hashtags) == 2);
        foreach ($post->hashtags as $index => $hashtag)
        {
            self::assertTrue($hashtag->text == $expectedHashtagTexts[$index]);
        }
    }

    public function testCommentPost_hasHashtagsBefore_addsHashtagAndKeepsOld()
    {
        $expectedCommentText = 'Comment text #hashtag1 #hashtag2';
        $user = User::factory()->create();
        $post = Post::factory()->create();
        $post->user()->associate($user)->save();
        $hashtags = Hashtag::factory()->count(5)->create();
        $post->hashtags()->saveMany($hashtags);
        $post->load([ 'hashtags' ]);
        $expectedHashTagTexts = Arr::flatten([ $hashtags->pluck('text'), 'hashtag1', 'hashtag2' ]);
        $this->mockRepository(PostRepository::class, [[
            "callCount" => 1,
            "method" => "getById",
            "rv" => $post
        ]]);
        $postService = app()->make(PostService::class);

        $this->actingAs($user);
        $postService->comment($user->id, $post->id, [ 'text' => $expectedCommentText ]);

        foreach ($post->hashtags as $index => $hashtag)
        {
            self::assertTrue($hashtag->text == $expectedHashTagTexts[$index]);
        }
    }

    public function testUpdatePost_postDoesNotExist_ThrowsNotFoundResourceException()
    {
        $this->mockRepository(PostRepository::class, [[
            "callCount" => 1,
            "method" => "getById",
            "rv" => null
        ]]);

        $postService = app()->make(PostService::class);

        $this->expectException(NotFoundResourceException::class);
        $postService->update(1, [  ]);
    }

    public function testParseHashtags_twoHashtags_Ok()
    {
        $description = "#hashtag1 #hashtag2";
        $expectedHashtags = [ "#hashtag1", "#hashtag2" ];

        $method = $this->getPublicCallableMethod(PostService::class, "parseHashtags");
        $postService = app()->make(PostService::class);
        $result = $method->invoke($postService, $description);

        self::assertTrue(in_array($expectedHashtags[0], $result));
        self::assertTrue(in_array($expectedHashtags[1], $result));
    }

    public function testParseHashtags_noHashtags_hashtagCount0()
    {
        $description = "This is description without hashtags.";
        $expectedHashtagCount = 0;

        $method = $this->getPublicCallableMethod(PostService::class, "parseHashtags");
        $postService = app()->make(PostService::class);
        $result = $method->invoke($postService, $description);

        self::assertCount($expectedHashtagCount, $result);
    }

    public function testParseHashtags_doubleHashtag_hashtagCount0()
    {
        $description = "This is description with double hashtags. ##";
        $expectedHashtagCount = 0;

        $method = $this->getPublicCallableMethod(PostService::class, "parseHashtags");
        $postService = app()->make(PostService::class);
        $result = $method->invoke($postService, $description);

        self::assertCount($expectedHashtagCount, $result);
    }

    public function testParseHashtags_hashtagNoTextAfter_hashtagCount0()
    {
        $description = "This is description with double hashtags. # something";
        $expectedHashtagCount = 0;

        $method = $this->getPublicCallableMethod(PostService::class, "parseHashtags");
        $postService = app()->make(PostService::class);
        $result = $method->invoke($postService, $description);

        self::assertCount($expectedHashtagCount, $result);
    }

    public function testParseHashtags_hashtagSpecialCharsAfter_hashtagCount0()
    {
        $description = "This is description with double hashtags. #./,;. something";
        $expectedHashtagCount = 0;

        $method = $this->getPublicCallableMethod(PostService::class, "parseHashtags");
        $postService = app()->make(PostService::class);
        $result = $method->invoke($postService, $description);

        self::assertCount($expectedHashtagCount, $result);
    }

}
