<?php


namespace Tests\Unit\app\Posts\Services;


use App\Posts\Repositories\CommentRepository;
use App\Posts\Repositories\PostRepository;
use App\Posts\Services\CommentService;
use App\Posts\Services\PostService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Tests\TestCase;

class CommentServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function testDestroyComment_CommentNotFound_ThrowsNotFoundResourceException()
    {
        $this->mockRepository(CommentRepository::class, [[
            "callCount" => 1,
            "method" => "getById",
            "rv" => null
        ]]);

        $commentService = app()->make(CommentService::class);

        $this->expectException(NotFoundResourceException::class);
        $commentService->destroy(1, [  ]);
    }
}
