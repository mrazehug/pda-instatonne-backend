<p align="center"><img src="https://instatonne.leafare.com/docs/images/logo.png" width="200"></p>

## Instatonne backend
Welcome to the repository for backend code of the Instatonne project. If you are looking to grade
TS1, there should be PDF document named TS_Document.pdf in root of this repo, or click <a href="https://gitlab.fel.cvut.cz/mrazehug/pda-instatonne-backend/-/blob/master/TS_Document.pdf">here</a>. If you are just snooping around, continue.
