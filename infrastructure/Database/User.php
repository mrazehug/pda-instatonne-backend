<?php

namespace Infrastructure\Database;

use App\Notifications\Model\Notification;
use App\Posts\Model\Comment;
use App\Posts\Model\Post;
use App\Users\Model\Follow;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Infrastructure\FileUploads\Model\FileUpload;
use Laravel\Passport\HasApiTokens;

class User extends Authenticable
{
    use HasApiTokens, HasFactory;

    protected $table = 'users';
    protected $fillable = [ 'username', 'email', 'password', 'bio' ];
    protected $hidden = [ 'password', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at', 'pivot', 'profilePhoto', 'profile_photo_id' ];
    protected $appends = [ 'profile_photo_url', 'is_following'];
    protected $with = [ 'profilePhoto' ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'follows', 'followed_id', 'follower_id')->withPivot('id')->using(Follow::class);
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'follows', 'follower_id', 'followed_id')->withPivot('id')->using(Follow::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function profilePhoto()
    {
        return $this->belongsTo(FileUpload::class, 'profile_photo_id');
    }

    public function getProfilePhotoUrlAttribute()
    {
        return $this->profilePhoto ? $this->profilePhoto->url : "";
    }

    public function getFollowerCountAttribute()
    {
        return count($this->followers);
    }

    public function getFollowingCountAttribute()
    {
        return count($this->following);
    }

    public function getResultTypeAttribute()
    {
        return "USER";
    }

    public function getisFollowingAttribute()
    {
        return $this->followers()->wherePivot('follower_id', Auth::id())->wherePivot('followed_id', $this->id)->first() != null;
    }

    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
