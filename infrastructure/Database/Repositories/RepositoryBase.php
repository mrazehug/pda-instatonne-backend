<?php


namespace Infrastructure\Database\Repositories;


use Infrastructure\Database\Eloquent\ModelBase;

class RepositoryBase
{
    protected string $model = ModelBase::class;

    public function getModelClass()
    {
        return $this->model;
    }

    public function getModel(): ModelBase
    {
        return app()->make($this->model);
    }

    public function create(array $data): ModelBase
    {
        $model = $this->getModel();
        $model->fill($data);

        return $model;
    }

    public function insert(array $data): ModelBase
    {
        $model = $this->getModel();
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function save(ModelBase $modelBase)
    {
        return $modelBase->save();
    }

    public function update(ModelBase $model, array $data): ModelBase
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete(ModelBase $model): bool
    {
        try {
            $model->delete();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function getWhere($where)
    {
        // TODO: Exception?
        if($this->model == '')
            return null;
        return app()->get($this->model)->where($where);
    }

    public function getAll()
    {
        return app()->get($this->model)->all();
    }

    public function whereIn($column, $in)
    {
        return app()->get($this->model)->whereIn($column, $in);
    }

    public function whereHas($relation, $closure) {
        return app()->get($this->model)->whereHas($relation, $closure);
    }

    public function getById($id)
    {
        return $this->getWhere([[ 'id', $id ]])->first();
    }
}
