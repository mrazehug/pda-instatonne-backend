<?php


namespace Infrastructure\FileUploads;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class FileUploadServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::group([ 'prefix' => 'api' ], base_path('infrastructure/FileUploads/api_routes.php'));
    }
}
