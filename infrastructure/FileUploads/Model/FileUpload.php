<?php


namespace Infrastructure\FileUploads\Model;


use Infrastructure\Database\Eloquent\ModelBase;

class FileUpload extends ModelBase
{
    protected $fillable = ['url'];
    public $timestamps = false;
    public $with = [ 'fileUploadType' ];

    public function getUrlAttribute($url)
    {
        return env('APP_URL').'/'.$url;
    }

    public function fileUploadType()
    {
        return $this->belongsTo(FileUploadType::class);
    }

    public function toArray()
    {
        return [
            "id" => $this->id,
            "url" => $this->url,
            // maybe will cause n+1 problem, but who cares
            "type" => $this->fileUploadType->value
        ];
    }
}
