<?php


namespace Infrastructure\FileUploads\Model;


use Infrastructure\Database\Eloquent\ModelBase;

class FileUploadType extends ModelBase
{
    const PHOTO_TYPE = 'PHOTO';
    const VIDEO_TYPE = 'VIDEO';

    public $timestamps = false;
}
