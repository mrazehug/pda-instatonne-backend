<?php


namespace Infrastructure\FileUploads\Repositories;


use Infrastructure\Database\Repositories\RepositoryBase;
use Infrastructure\FileUploads\Model\FileUpload;

class FileUploadRepository extends RepositoryBase
{
    protected string $model = FileUpload::class;
}
