<?php

use Illuminate\Support\Facades\Route;
use Infrastructure\FileUploads\Controllers\FileUploadController;

Route::resource('file-uploads', FileUploadController::class)
    ->only(['index', 'store', 'destroy']);
