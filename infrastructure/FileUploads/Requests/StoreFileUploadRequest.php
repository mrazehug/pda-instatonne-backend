<?php


namespace Infrastructure\FileUploads\Requests;


use Infrastructure\Http\Requests\RequestBase;

class StoreFileUploadRequest extends RequestBase
{
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:jpeg,png,bmp,mp4,avi'
        ];
    }
}
