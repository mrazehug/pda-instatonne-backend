<?php

namespace Infrastructure\Auth\Controllers\Web;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    private $apiLoginController;

    public function __construct(\Infrastructure\Auth\Controllers\Api\LoginController $apiLoginController)
    {
        $this->apiLoginController = $apiLoginController;
        $this->middleware('guest')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $this->apiLoginController->login($request);
    }
}
