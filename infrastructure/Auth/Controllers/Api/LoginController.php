<?php


namespace Infrastructure\Auth\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use Infrastructure\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * @group Auth
 *
 * Authorization endpoints
 */

class LoginController extends Controller
{
    const REFRESH_TOKEN = 'refresh_token';

    private $cookie;

    public function __construct()
    {
        $this->cookie = app()->make('cookie');
    }

    /**
     * Login
     *
     * Logs in user.
     *
     * @bodyParam username string required username of the user to log in
     * @bodyParam password string required password of the user to log in
     *
     * @responseFile responses/auth/login.json
     */
    public function login(Request $request)
    {
        $data = $request->all();
        $username = $request->get('email');
        $password = $request->get('password');

        return $this->proxy('password', [
            "username" => $username,
            "password" => $password
        ]);
    }

    public function proxy($grantType, array $data = [])
    {
        $data = array_merge($data, [
            'client_id'     => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'grant_type'    => $grantType
        ]);

        // error_log(print_r($data, true));

        $request = Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($request);

        // error_log(print_r(json_decode($response->getContent()), true));


        if ($response->getStatusCode() != 200) {
            throw new UnauthorizedException();
        }

        $data = json_decode($response->getContent());

        $this->cookie->queue(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            864000, // 10 days
            '/api',
            '.localhost',
            false,
            true // HttpOnly
        );

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ];
    }
}
