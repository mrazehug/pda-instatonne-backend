<?php

namespace Infrastructure\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Passport\Exceptions\OAuthServerException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|object|void
     */
//    public function register()
//    {
//        $this->reportable(function (Throwable $e) {
//            //
//        });
//    }

//    public function response($message, $statusCode)
//    {
//        return response([ 'message' => $message ])->setStatusCode($statusCode);
//    }

    public function render($request, Throwable $e)
    {
        // return response([ 'message' => $e->getMessage() ])->setStatusCode(500);
        // return parent::render($request, $e);
        if($e instanceof ValidationException)
        {
            return response([ 'message' => $e->getMessage(), 'errors' => $e->errors() ])->setStatusCode(422);
        }

        if ($e instanceof AuthorizationException)
        {
            return response([ 'message' => "You are not allowed to perform this action." ])->setStatusCode(403);
        }

        if (   $e instanceof UnauthorizedException
            || $e instanceof OAuthServerException
            || $e instanceof AuthenticationException)
        {
            return response([ 'message' => "Log in to access this route." ])->setStatusCode(401);
        }

        if ( $e instanceof NotFoundResourceException)
        {
            return response([ 'message' => "Resource not found." ])->setStatusCode(404);
        }

        if ( $e instanceof NotFoundHttpException )
        {
            return response([ 'message' => "Route not found." ])->setStatusCode(404);
        }

        return parent::render($request, $e);
    }
}
