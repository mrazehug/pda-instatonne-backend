<?php


namespace Infrastructure\Http\Services;


use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Support\Arr;
use Infrastructure\Database\Eloquent\ModelBase;
use Infrastructure\Database\Repositories\RepositoryBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

abstract class ServiceBase
{
    protected RepositoryBase $repository;
    protected string $identifierColumnName = 'id';

    function index(array $options)
    {
        return $this->repository->getAll();
    }

    function show($identifier, array $include = [])
    {
        return $this->getRequestedModel($identifier, $include);
    }

    function store(array $data): ModelBase
    {
        return $this->repository->insert($data);
    }

    function update($identifier, array $data): ModelBase
    {
        $model = $this->getRequestedModel($identifier);
        return $this->repository->update($model, $data);
    }

    function destroy($identifier)
    {
        $model = $this->getRequestedModel($identifier);
        return $this->repository->delete($model);
    }

    public function getRequestedModel($identifier, array $include = [])
    {
        // Construct query
        $model = null;
        if(count($include) == 0)
        {
            if($this->identifierColumnName == 'id')
                $model = $this->repository->getById($identifier);
            else
                $model = $this->repository->getWhere([[$this->identifierColumnName, $identifier]])->first();
        }
        else
        {
            $query = $this->repository->getWhere([[ $this->identifierColumnName, $identifier ]]);
            $query->with($include);
            $model = $query->first();
        }

        // TODO: find correct exception
        if(!$model)
            throw new NotFoundResourceException();
        return $model;
    }

    public function setIdentifierColumnName($identifierColumnName)
    {
        $this->identifierColumnName = $identifierColumnName;
    }
}
