<?php


namespace Infrastructure\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Test candidate, rules could be mocked.
 *
 * Class RequestBase
 * @package Infrastructure\Http\Requests
 */
class RequestBase extends FormRequest
{
    public function getData() : array
    {
        return $this->camelCaseKeys($this->validated());
    }

    private function camelCaseKeys($apiResponseArray)
    {
        $arr = [];
        foreach ($apiResponseArray as $key => $value) {
            // split into words, uppercase their first letter, join them,
            // lowercase the very first letter of the name
            $key = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));

            if (is_array($value))
                $value = $this->camelCaseKeys($value);

            $arr[$key] = $value;
        }
        return $arr;
    }
}
