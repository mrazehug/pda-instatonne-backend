---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)

<!-- END_INFO -->

#Auth


Authorization endpoints
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login

Logs in user.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs" \
    -d '{"username":"adipisci","password":"omnis"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

let body = {
    "username": "adipisci",
    "password": "omnis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmIzN2JhN2JlMmNlNDM0YmM4NjE4MDA3N2I3MWQxZDg5MGM0YTJhYzgxZTNkNmUwZWUzOGJjMzk0MDE5MDU4OTY0ZjY2ZmQzZTQ2MDQ4NDEiLCJpYXQiOjE2MjAxNDc0NDguMTYxODQ3LCJuYmYiOjE2MjAxNDc0NDguMTYxODY4LCJleHAiOjE2NTE2ODM0NDcuODQ0MjI2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Ul4NSaHdnoxWLkE13tQgmwigsaro7CIEmTVrvIrsWAMlOsHmAoF75Fs-NDHaukxlS8Tj3y6EH-z0qMBOTnVCH33o4QVrPd0cok_qw0zi0ZuVDRfjF9RrPkzRWvfw3OC-lsTZO0-GoW7gR31-4AnZYgA02L7onQWMWwUwbWg-f_FY6DSqWcCalp0wPBbCtcPLJT8AZ26ev7SfqkAysng_Vk0hrNqjFROknwjSApMFDBaFWAppnkMzZl7g72MKu9TVxeoUSH0YRa4v0qrDKjA8LM5NEKcqdzBlVlkz5ipi2yIRhS2VbQZ7szT7Cw6f3r9X2bsQTP_PnZHzsqEhFv5cWLI5NSnl7nwchgplTEkNh3W1oq-rpU8MaTqBOSVSyKNH8lP2EzwbUHmPgvQxi22kVTPj2q4CI7VXbLUEZMZ0gvs_IYjBYKwrFUCpdvT0-uOOOZyFuAgTr2pdoLh0sviaSI94qlz2DAtVAu8Me5ID6o73qe7dN1yQcPJfixmm-bFq3WsuHkVf2kUVIwcTgOtxf4V7bmNC9G4Z98D9HgLee2V5-25aAaxuYTHlzA0FuCs4aFfueNfVEBF_ujupIV4qg_ljkyh50dt1em3sGCc3Bawwm5HS222NGJK4uhXyT8avsAsp6HCE3UnksHf-_qzf9UV42l866CPJyaNP7mmJM4o",
    "expires_in": 31535999
}
```

### HTTP Request
`POST api/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username of the user to log in
        `password` | string |  required  | password of the user to log in
    
<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Register
Registers user.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs" \
    -d '{"username":"ipsam","email":"sunt","password":"voluptatem","password_confirmation":"voluptatem"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

let body = {
    "username": "ipsam",
    "email": "sunt",
    "password": "voluptatem",
    "password_confirmation": "voluptatem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/register`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username of the user to register
        `email` | email |  required  | email of the user to register
        `password` | string |  required  | password of the user to register
        `password_confirmation` | string |  required  | password confirmation
    
<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

#Comments

API for managing comments

Class CommentController
<!-- START_a446ee5cc043f690570906c492c40786 -->
## Destroy comment
Destroys reqested comment

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/comments/dolore" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/comments/dolore"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/comments/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | id of the comment to delete

<!-- END_a446ee5cc043f690570906c492c40786 -->

#Me

Endpoint for managing currently authenticated user.
<!-- START_b19e2ecbb41b5fa6802edaf581aab5f6 -->
## Me
Returns information about currently authenticated user.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "username": "john_doe",
        "email": "john.doe@gmail.com",
        "bio": "My bio",
        "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png",
        "followerCount": 1,
        "followingCount": 1,
        "posts": [
            {
                "id": 1,
                "created_at": "2021-05-04T12:47:33.000000Z",
                "description": "whatever #w #test"
            },
            {
                "id": 2,
                "created_at": "2021-05-04T16:41:42.000000Z",
                "description": "whatever #w #test"
            },
            {
                "id": 3,
                "created_at": "2021-05-04T16:41:53.000000Z",
                "description": "whatever #w #test"
            }
        ],
        "followers": [
            {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        ],
        "following": [
            {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        ]
    }
}
```

### HTTP Request
`GET api/me`


<!-- END_b19e2ecbb41b5fa6802edaf581aab5f6 -->

<!-- START_fa77e70040eb60f0488db2d285d1cdc7 -->
## Update me
Updates currently authenticated user. (Does not work yet)

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs" \
    -d '{"username":"et","email":"pariatur"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

let body = {
    "username": "et",
    "email": "pariatur"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "username": "john_doe",
        "bio": "My bio",
        "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
    }
}
```

### HTTP Request
`PUT api/me`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  optional  | New username of the user.
        `email` | string |  optional  | New username of the user.
    
<!-- END_fa77e70040eb60f0488db2d285d1cdc7 -->

<!-- START_0cdd2daf20dd8b9f5e1a39a86e9f222b -->
## My feed
Returns feed of currently authenticated user.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/me/feed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/me/feed"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "created_at": "2021-05-04T12:47:33.000000Z",
            "description": "whatever #w #test",
            "commentCount": 2,
            "likeCount": 1,
            "comments": [
                {
                    "id": 2,
                    "text": "Hella lit post! #cool",
                    "user_id": 1,
                    "post_id": 1,
                    "created_at": "2021-05-04T13:25:18.000000Z",
                    "updated_at": "2021-05-04T13:25:18.000000Z",
                    "deleted_at": null
                },
                {
                    "id": 3,
                    "text": "Hella lit post! #cool",
                    "user_id": 1,
                    "post_id": 1,
                    "created_at": "2021-05-04T13:32:37.000000Z",
                    "updated_at": "2021-05-04T13:32:37.000000Z",
                    "deleted_at": null
                }
            ],
            "file_upload": {
                "id": 1,
                "url": "http:\/\/localhost:8000\/uploads\/1620132453.png",
                "type": "PHOTO"
            },
            "user": {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        }
    ]
}
```

### HTTP Request
`GET api/me/feed`


<!-- END_0cdd2daf20dd8b9f5e1a39a86e9f222b -->

<!-- START_28097fe1e2a573f97686edd86e90d373 -->
## My notifications
Returns list of notifications for currently authenticated user.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/me/notifications" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/me/notifications"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 14,
            "type": "LIKE",
            "like": {
                "post": {
                    "id": 1,
                    "created_at": "2021-05-04T12:47:33.000000Z",
                    "description": "whatever #w #test",
                    "file_upload": {
                        "id": 1,
                        "url": "http:\/\/localhost:8000\/uploads\/1620132453.png",
                        "type": "PHOTO"
                    }
                },
                "user": {
                    "id": 1,
                    "username": "hugo_changed",
                    "bio": "Popisek",
                    "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
                }
            },
            "created_at": "2021-05-04T13:32:28.000000Z"
        },
        {
            "id": 15,
            "type": "COMMENT",
            "comment": {
                "post": {
                    "id": 1,
                    "created_at": "2021-05-04T12:47:33.000000Z",
                    "description": "whatever #w #test",
                    "file_upload": {
                        "id": 1,
                        "url": "http:\/\/localhost:8000\/uploads\/1620132453.png",
                        "type": "PHOTO"
                    }
                },
                "user": {
                    "id": 1,
                    "username": "hugo_changed",
                    "bio": "Popisek",
                    "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
                }
            },
            "created_at": "2021-05-04T13:32:38.000000Z"
        },
        {
            "id": 16,
            "type": "FOLLOW",
            "follow": {
                "user": {
                    "id": 1,
                    "username": "hugo_changed",
                    "bio": "Popisek",
                    "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
                }
            },
            "created_at": "2021-05-04T15:59:19.000000Z"
        }
    ]
}
```

### HTTP Request
`GET api/me/notifications`


<!-- END_28097fe1e2a573f97686edd86e90d373 -->

<!-- START_bf18d87b4589529bd2f04302e4437777 -->
## Store my post
Stores post of currently authenticated user

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/me/posts" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs" \
    -d '{"file":"est","description":"adipisci"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/me/posts"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

let body = {
    "file": "est",
    "description": "adipisci"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "description": "whatever #w #test",
        "created_at": "2021-05-04T16:41:53.000000Z",
        "id": 3,
        "file_upload": {
            "id": 6,
            "url": "http:\/\/localhost:8000\/uploads\/1620146513.png",
            "type": "PHOTO"
        },
        "comments": []
    }
}
```

### HTTP Request
`POST api/me/posts`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | file |  required  | The file of the post. Accepts only pictures now.
        `description` | string |  required  | The description of the post. Max length: 1024
    
<!-- END_bf18d87b4589529bd2f04302e4437777 -->

#Posts


APIs for posts
<!-- START_da50450f1df5336c2a14a7a368c5fb9c -->
## Index posts

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/posts" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/posts"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "created_at": "2021-05-04T12:47:33.000000Z",
            "description": "whatever #w #test",
            "user": {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            },
            "file_upload": {
                "id": 1,
                "url": "http:\/\/localhost:8000\/uploads\/1620132453.png",
                "type": "PHOTO"
            },
            "hashtags": [
                {
                    "id": 1,
                    "text": "w"
                },
                {
                    "id": 2,
                    "text": "test"
                },
                {
                    "id": 3,
                    "text": "cool"
                }
            ]
        },
        {
            "id": 2,
            "created_at": "2021-05-04T16:41:42.000000Z",
            "description": "whatever #w #test",
            "user": {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            },
            "file_upload": {
                "id": 5,
                "url": "http:\/\/localhost:8000\/uploads\/1620146502.png",
                "type": "PHOTO"
            },
            "hashtags": [
                {
                    "id": 1,
                    "text": "w"
                },
                {
                    "id": 2,
                    "text": "test"
                }
            ]
        },
        {
            "id": 3,
            "created_at": "2021-05-04T16:41:53.000000Z",
            "description": "whatever #w #test",
            "user": {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            },
            "file_upload": {
                "id": 6,
                "url": "http:\/\/localhost:8000\/uploads\/1620146513.png",
                "type": "PHOTO"
            },
            "hashtags": [
                {
                    "id": 1,
                    "text": "w"
                },
                {
                    "id": 2,
                    "text": "test"
                }
            ]
        }
    ]
}
```

### HTTP Request
`GET api/posts`


<!-- END_da50450f1df5336c2a14a7a368c5fb9c -->

<!-- START_6a70fcda407b5f1a7ff2809c4ebff500 -->
## Show post
Shows requested post

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/posts/quam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/posts/quam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "created_at": "2021-05-04T12:47:33.000000Z",
        "description": "whatever #w #test",
        "likeCount": 1,
        "commentCount": 1,
        "user": {
            "id": 1,
            "username": "john_doe",
            "bio": "My bio",
            "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
        },
        "file_upload": {
            "id": 1,
            "url": "http:\/\/localhost:8000\/uploads\/1620132453.png",
            "type": "PHOTO"
        },
        "hashtags": [
            {
                "id": 1,
                "text": "w"
            },
            {
                "id": 2,
                "text": "test"
            },
            {
                "id": 3,
                "text": "cool"
            }
        ],
        "likes": [
            {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        ],
        "comments": [
            {
                "id": 3,
                "text": "Hella lit post! #cool",
                "created_at": "2021-05-04T13:32:37.000000Z",
                "deleted_at": null
            }
        ]
    }
}
```

### HTTP Request
`GET api/posts/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | id of the post to show

<!-- END_6a70fcda407b5f1a7ff2809c4ebff500 -->

<!-- START_1849a8b932388e4e89cdbf8e7d8de62b -->
## Like post
Toggles like status between post and currently authenticated user.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/posts/rem/like" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/posts/rem/like"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": true
}
```

### HTTP Request
`POST api/posts/{id}/like`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | id of the post to like/dislike

<!-- END_1849a8b932388e4e89cdbf8e7d8de62b -->

<!-- START_6c88da04b51f0318abef69d7ef35dce0 -->
## Comment post
Adds comment to a post.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/posts/nobis/comment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs" \
    -d '{"text":"et"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/posts/nobis/comment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

let body = {
    "text": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "text": "Hella lit post! #cool",
        "created_at": "2021-05-04T16:45:52.000000Z",
        "id": 7
    }
}
```

### HTTP Request
`POST api/posts/{id}/comment`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | id of the post to comment
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `text` | string |  required  | Text of the comment. Max length 2048
    
<!-- END_6c88da04b51f0318abef69d7ef35dce0 -->

#Search


<!-- START_f7828fe70326ce6166fdba9c0c9d80ed -->
## Search

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/search?lf=aut&q=nemo" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/search"
);

let params = {
    "lf": "aut",
    "q": "nemo",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "username": "john_doe",
            "bio": "My bio",
            "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png",
            "result_type": "USER",
            "follower_count": 1,
            "following_count": 1,
            "followers": [
                {
                    "id": 1,
                    "username": "john_doe",
                    "bio": "My bio",
                    "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
                }
            ],
            "following": [
                {
                    "id": 1,
                    "username": "john_doe",
                    "bio": "My bio",
                    "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
                }
            ]
        },
        {
            "id": 1,
            "text": "w",
            "result_type": "HASHTAG",
            "posts": [
                {
                    "id": 1,
                    "created_at": "2021-05-04T12:47:33.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 1,
                        "post_id": 1
                    }
                },
                {
                    "id": 2,
                    "created_at": "2021-05-04T16:41:42.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 1,
                        "post_id": 2
                    }
                },
                {
                    "id": 3,
                    "created_at": "2021-05-04T16:41:53.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 1,
                        "post_id": 3
                    }
                }
            ]
        },
        {
            "id": 2,
            "text": "test",
            "result_type": "HASHTAG",
            "posts": [
                {
                    "id": 1,
                    "created_at": "2021-05-04T12:47:33.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 2,
                        "post_id": 1
                    }
                },
                {
                    "id": 2,
                    "created_at": "2021-05-04T16:41:42.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 2,
                        "post_id": 2
                    }
                },
                {
                    "id": 3,
                    "created_at": "2021-05-04T16:41:53.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 2,
                        "post_id": 3
                    }
                }
            ]
        },
        {
            "id": 3,
            "text": "cool",
            "result_type": "HASHTAG",
            "posts": [
                {
                    "id": 1,
                    "created_at": "2021-05-04T12:47:33.000000Z",
                    "description": "whatever #w #test",
                    "pivot": {
                        "hashtag_id": 3,
                        "post_id": 1
                    }
                }
            ]
        }
    ]
}
```

### HTTP Request
`GET api/search`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `lf` |  optional  | Looking for - specifies, what you are looking for. Acceptable values: HASHTAG, USER
    `q` |  optional  | Query - the search term you are looking for.

<!-- END_f7828fe70326ce6166fdba9c0c9d80ed -->

#Users


APIs for users
<!-- START_d56f7f4f61fed72d7d9219a5a8ab0b44 -->
## Show user
Shows user with the given id.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/users/dolorum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/users/dolorum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "username": "john_doe",
        "bio": "My bio",
        "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png",
        "followerCount": 1,
        "followingCount": 1,
        "posts": [
            {
                "id": 1,
                "created_at": "2021-05-04T12:47:33.000000Z",
                "description": "whatever #w #test"
            },
            {
                "id": 2,
                "created_at": "2021-05-04T16:41:42.000000Z",
                "description": "whatever #w #test"
            },
            {
                "id": 3,
                "created_at": "2021-05-04T16:41:53.000000Z",
                "description": "whatever #w #test"
            }
        ],
        "followers": [
            {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        ],
        "following": [
            {
                "id": 1,
                "username": "john_doe",
                "bio": "My bio",
                "profile_photo_url": "http:\/\/localhost:8000\/uploads\/1620145231.png"
            }
        ]
    }
}
```

### HTTP Request
`GET api/users/{username}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `username` |  optional  | username of the user to show.

<!-- END_d56f7f4f61fed72d7d9219a5a8ab0b44 -->

<!-- START_a6553249a2e85748aa65e5de595e7c8b -->
## Follow
Toggles follow between currently authenticated user and user with given username

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/users/magnam/follow" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/users/magnam/follow"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDI4YzU0Yjg1YzVlODNkZGMxZWM4MDRjMzAwODIwYjAzYjY5NjZjZjYzY2Y4YmVkZjlmYzNiNTMxMWVhZDdjMzgzNmFkYjRiMTk3ZDAyOGQiLCJpYXQiOjE2MjAxMzI0MzMuNTExODQ0LCJuYmYiOjE2MjAxMzI0MzMuNTExODQ4LCJleHAiOjE2NTE2Njg0MzMuMjk1Nzk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.rosb-RF_rVY-dQjK1fHc6KUGq7VGg_SfoslhsEWCbcZ3XWjPl9zNLFPRoFmZ0Is9IYkiKNXXcwrOtrilxmDGMuCirfD0hnLyiBicFVUnM8Z3ecHAApez20IEU3CSaSC8R1E_Lo6VcJdZkH-rfTkKTMN5DJsN2VOf2xLomxprC5-0DLxt7iz2q4rM6As4a9ykmvfSYWXtrHOWuN9zq4U9BLQnqDCC8YuQh1jJYCRsDtS45yr8frMydw5ffty1c0BNGKJbXF8Vn4fyupAS3xUzLHYcd_UnzgfXPs4PGBD9cBM2tT48-xb20IWU9MuGYry-6h7Whl64QZ1hpB6BuMqsy7z-x2atvQyjdCakCUfmvKenPPPrUhoZCqAwy-C3zFDnuTgCqAz1vmPDKvIcUBTUIm0x94nfHO5wQ_n-saC_hsbiUGjbg0c7gvPyg1O6ylQGqbynuD0WR41dtgkJ06G4NCSOwg5exU1n45voqKdQrq6VIp1NqIB6xNeVPqjEBchPdJ9i2l7sdJKBTgOYwI_jStJuLdSjwAI7llgN8nuUflkQQ8Q1OeavkrNte7iwTrNafocmIFc57Blr-_C-42aqff-gnATmmS0rptYJyDb9-4WaI6353SYmiTfWaJKkmlbBv8bm9_tLNYw2IsdM1-aLLWAgPI02S1JyVwmxi3POKgs",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": true
}
```

### HTTP Request
`POST api/users/{username}/follow`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `username` |  optional  | username of the user to follow/unfollow

<!-- END_a6553249a2e85748aa65e5de595e7c8b -->


