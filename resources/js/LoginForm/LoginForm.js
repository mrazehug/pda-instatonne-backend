import React from "react";
import ReactDOM from "react-dom";
import {useForm} from "react-hook-form";
import axios from "axios";

const LoginForm = () => {
    const { handleSubmit, register } = useForm();

    const onSuccess = (data) => {
        console.log(data);
        axios.post("/login", data).then((response)=>{
            console.log(response);
        });
    }

    const onError = (errors) => {
        console.error(errors);
    }

    return <div>
        <form onSubmit={handleSubmit(onSuccess, onError)}>
            <label>Email</label>
            <input name={"email"} ref={register}/>
            <label>Password</label>
            <input name={"password"} ref={register}/>
            <button>Submit</button>
        </form>
    </div>
}

ReactDOM.render(<LoginForm/>, document.getElementById('login-form'));
