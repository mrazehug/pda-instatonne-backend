<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Infrastructure\Auth\Controllers\Web\LoginController;
use Infrastructure\Auth\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

 // Route::get('/login', [LoginController::class, 'showLoginForm']);
 // Route::post('/login', [LoginController::class, 'login']);

 // Route::get('/register', [RegisterController::class, 'showRegistrationForm']);
 // Route::post('/register', [RegisterController::class, 'register']);

